"""
A class to quickly manipulate an optic in the alignment guardian script. Not 
sure if this is the best idea but I wrote it in the end.

Author: Nathan Holland
Contact nathan.holland@ligo.org
Date: 2019-05-07

Modified: 2019-05-07 (Preliminary coding completed, no debugging)
Modified: 2019-05-09 (Some debugging done,
                      The Optic class will only work when the variable ezca IS 
                      IN SCOPE as an instance of ezca.Ezca, as setup by 
                      Guardian.).
Modified: 2019-05-23 (Debugging on X2 LLO).
Modified: 2019-05-24 (Debugging on X2 LLO).
Modified: 2019-06-07 (Fixed ezca feedthrough for align_restore function).
"""
#-------------------------------------------------------------------------------
#Imports:

import sys
sys.path.append('/opt/rtcds/userapps/trunk/sus/common/scripts')

import time;
from new_align_restore import align_restore;
    #Import the function align_restore from 
    #/opt/rtcds/userapps/release/sus/common/scripts/new_align_restore.py

from new_align_save import align_save;
    #Import the function align_save from
    #/opt/rtcds/userapps/release/sus/common/scripts/new_align_save.py
#-------------------------------------------------------------------------------
class Optic(object):
    """
    A convenience class to simplify a lot of the reused code. Note that it isn't
     intended to be fully comprehensive but is designed to reduce the amount of 
    repeated code.
    
    This is intended for use ONLY in contexts where the variable ezca, for EPICS
     access is ALREADY defined. One such example would be in guardian scripts.
    
    For the ITMs/ETMs it is fixed to M0. For other optics it is fixed to M1. 
    Deals only with TEST and OPTICALIGN filters. Allows access to the gain, ramp
     time and offset.
    
    Intended for use with ITMs, ETMs, SRMs, PRMs, and the BS.
    """
    def __init__(self, name):
        """
        Initialisation of the Optic class.
        
        Usage:
        obj = Optic(name)
        obj       - The object, an instance of the Optic class.
        name      - The name of the optic, eg. ITMX.
        """
        self._optic = name;
            #The name of the optic.
        #
        if name[1:3] == "TM":
            #ITMX, ITMY, ETMX, and ETMY - move M0.
            mass = "M0";
        else:
            #BS;
            #TMSX, TMSY;
            #PRM, PR2, PR3;
            #SRM, SR2, and SR3 - move M1.
            mass = "M1";
        #
        self._test_FILT_name = ":SUS-" + name + "_{0}_TEST_".format(mass) + \
                               "{0}";
        self._test_GAIN_chan = ":SUS-" + name + "_{0}_TEST_".format(mass) + \
                               "{0}_GAIN";
        self._test_TRAMP_chan = ":SUS-" + name + "_{0}_TEST_".format(mass) + \
                                "{0}_TRAMP";
        self._test_OFFSET_chan = ":SUS-" + name + "_{0}_TEST_".format(mass) + \
                                 "{0}_OFFSET";
            #Partial channel names for the DOF test filters.
        #
        self._optic_align_FILT_name = ":SUS-" + name + \
                                      "_{0}_OPTICALIGN_".format(mass) + "{0}";
        self._optic_align_GAIN_chan = ":SUS-" + name + \
                                      "_{0}_OPTICALIGN_".format(mass) + \
                                      "{0}_GAIN";
        self._optic_align_TRAMP_chan = ":SUS-" + name + \
                                       "_{0}_OPTICALIGN_".format(mass) + \
                                       "{0}_TRAMP";
        self._optic_align_OFFSET_chan = ":SUS-" + name + \
                                        "_{0}_OPTICALIGN_".format(mass) + \
                                        "{0}_OFFSET";
            #Partial channel names for the DOF optic align filters.
    #
    
    def test_gain(self, dof, gain, ezca):
        """
        Set the gain of the optic's test filter.
        
        Usage:
        optic_obj.test_gain(dof, gain, ezca)
        optic_obj - An instance of the Optic class.
        dof       - The degree of freedom, of the filter.
        gain      - The gain to set the filter bank to.
        ezca      - Pass through for ezca.Ezca() object (EPICS channel access).
        """
        #nonlocal ezca;
            #Would work, in Python 3, to access the ezca in the 
            #immediately higher scope.
        ezca.write(self._test_GAIN_chan.format(dof), gain);
    #
    
    def copy_oagain_to_test(self, dof, ezca):
        """
        Set the gain of the test bank to match the optic align bank

        Usage:
        optic_obj.copy_oagain_to_test(dof, ezca)
        dof       - The degree of freedom, of the filter.
        ezca      - Pass through for ezca.Ezca() object (EPICS channel access).
        """
        self.test_gain(dof, \
            ezca.read(self._optic_align_GAIN_chan.format(dof)), ezca);


    def optic_align_gain(self, dof, gain, ezca):
        """
        Set the gain of the optic's optic align filter.
        
        Usage:
        optic_obj.optic_align_gain(dof, gain, ezca)
        optic_obj - An instance of the Optic class.
        dof       - The degree of freedom, of the filter.
        gain      - The gain to set the filter bank to.
        ezca      - Pass through for ezca.Ezca() object (EPICS channel access).
        """
        ezca.write(self._optic_align_GAIN_chan.format(dof), gain);
    #
    
    def test_ramp(self, dof, time, ezca):
        """
        Set the ramp time of the optic's test filter.
        
        Usage:
        optic_obj.test_ramp(dof, time, ezca)
        optic_obj - An instance of the Optic class.
        dof       - The degree of freedom, of the filter.
        time      - The ramp time to set the filter bank to have.
        ezca      - Pass through for ezca.Ezca() object (EPICS channel access).
        """
        ezca.write(self._test_TRAMP_chan.format(dof), time);
    #
    
    def optic_align_ramp(self, dof, time, ezca):
        """
        Set the ramp time of the optic's optic align filter.
        
        Usage:
        optic_obj.optic_align_ramp(dof, time, ezca)
        optic_obj - An instance of the Optic class.
        dof       - The degree of freedom, of the filter.
        time      - The ramp time to set the filter bank to have.
        ezca      - Pass through for ezca.Ezca() object (EPICS channel access).
        """
        ezca.write(self._optic_align_TRAMP_chan.format(dof), time);
    #
    
    def test_offset(self, dof, offset, ezca):
        """
        Set the offset of the optic's test filter.
        
        Usage:
        optic_obj.test_offset(dof, offset, ezca)
        optic_obj - An instance of the Optic class.
        dof       - The degree of freedom, of the filter.
        offset    - The offset to set the filter bank to.
        ezca      - Pass through for ezca.Ezca() object (EPICS channel access).
        """
        ezca.write(self._test_OFFSET_chan.format(dof), offset);
    #
    
    def optic_align_offset(self, dof, offset, ezca):
        """
        Set the offset of the optic's optic align filter.
        
        Usage:
        optic_obj.optic_align_offset(dof, offset, ezca)
        optic_obj - An instance of the Optic class.
        dof       - The degree of freedom, of the filter.
        offset    - The offset to set the filter bank to.
        ezca      - Pass through for ezca.Ezca() object (EPICS channel access).
        """
        ezca.write(self._optic_align_OFFSET_chan.format(dof), offset);
    #
    
    def misalign(self, dof, offset, ezca):
        """
        Misalign a specifed DoF for the optic by a specified amount.
        
        Usage:
        optic_obj.misalign(dof, offset, ezca)
        optic_obj - An instance of the Optic class.
        dof       - The degree of freedom to misalign.
        offset    - The amount to misalign the optic by.
        ezca      - Pass through for ezca.Ezca() object (EPICS channel access).
        """
        self.test_gain(dof, \
            ezca.read(self._optic_align_GAIN_chan.format(dof)), ezca);
            #Set the test gain to have the same value as the optic align filter.
        self.test_ramp(dof, 5.0, ezca);
            #Set the tramp time to be 5 seconds.
        #
        time.sleep(1.0);
            #Pause for 1 second, to let the changes take effect.
        #
        self.test_offset(dof, offset, ezca);
            #Apply the offset.
        if self._test_FILT_name.format(dof)[0] == ':':
            filt = ezca.get_LIGOFilter(self._test_FILT_name.format(dof)[1:]);
                #Get the LIGO filter (ezca.ligofilter.LIGOFilter instance).
                #Slice of the leading :, if it exists, to avoid L1::... as 
                #the filter name.
        else:
            filt = ezca.get_LIGOFilter(self._test_FILT_name.format(dof));
                #Get the LIGO filter (ezca.ligofilter.LIGOFilter instance).
        #
        filt.switch_on("OUTPUT", "OFFSET");
            #Switch the output, and offset for the filter on.
    #
    
    def align(self, dof, ezca):
        """
        Align a misaligned DoF for the specified optic.
        
        Usage:
        optic_obj.align(dof, ezca)
        optic_obj - An instance of the Optic class.
        dof       - The degree of freedom to align.
        ezca      - Pass through for ezca.Ezca() object (EPICS channel access).
        """
        self.test_ramp(dof, 5.0, ezca);
            #Set the ramp time for the test filter.
        #self.optic_align_ramp(dof, 5.0, ezca);
            #Set the ramp time for the optic align filter.
        #
        time.sleep(1.0);
            #Pause for 1 second, to let the changes take effect.
        #
        self.test_offset(dof, 0.0, ezca);
            #Remove any offsets.
        if self._test_FILT_name.format(dof)[0] == ':':
            filt = ezca.get_LIGOFilter(self._test_FILT_name.format(dof)[1:]);
                #Get the LIGO filter (ezca.ligofilter.LIGOFilter instance).
                #Slice of the leading :, if it exists, to avoid L1::... as 
                #the filter name.
        else:
            filt = ezca.get_LIGOFilter(self._test_FILT_name.format(dof));
                #Get the LIGO filter (ezca.ligofilter.LIGOFilter instance).
        #
        filt.switch_off("OFFSET");
            #Switch the offset for the test filter off.
        #
        #align_restore(self._optic, ezca);
            #Restore the optic's position.
    #

    def is_misaligned(self, dof, ezca):
        """
        Check if optic is misaligned, i.e. are test offsets on
        """
        if self._test_FILT_name.format(dof)[0] == ':':
            filt = ezca.get_LIGOFilter(self._test_FILT_name.format(dof)[1:]);
                #Get the LIGO filter (ezca.ligofilter.LIGOFilter instance).
                #Slice of the leading :, if it exists, to avoid L1::... as 
                #the filter name.
        else:
            filt = ezca.get_LIGOFilter(self._test_FILT_name.format(dof));
                #Get the LIGO filter (ezca.ligofilter.LIGOFilter instance).

        return filt.is_engaged('OFFSET')


    def restore(self, ezca):
        """
        Restore the saved value for the specified optic.
        """
        align_restore(self._optic, ezca);
    #

    def save(self, ezca):
        """
        Save the value for the specified optic.
        """
        align_save(self._optic, ezca)

#

