#-------------------------------------------------------------------------------
#Docstring:
"""
Initial Alignment Guardian: Initial Alignment Guardian.

This guardian manages the other inital alignment guardians to guide the IFO 
through the inital alignment process.

Author: Nathan Holland, A. Mullavey.
Date: 2019-05-14
Contact: nathan.holland@ligo.org

Modified: 2019-05-14 (Created).
Modified: 2019-05-22 (Changed structure to make it more self contained).
"""
#-------------------------------------------------------------------------------
#Imports:
import numpy as np;
#
from guardian import GuardState, GuardStateDecorator, NodeManager, Node;
import cdsutils;
import time

# Import the class Optic from optic.py
# Source @
# /opt/rtcds/userapps/release/asc/l1/guardian/optic.py
from optic import Optic

# Import the function align_restore from new_align_restore.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/new_align_restore.py
from new_align_restore import align_restore

# Import the function align_save from new_align_save.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/align_save.py
from new_align_save import align_save

#
#-------------------------------------------------------------------------------
#Script Variables:
nominal = "INITIAL_ALIGN_IDLE";
request = nominal

# Convenient access for QUADs and HSTSs.
itmx = Optic("ITMX")
etmx = Optic("ETMX")
itmy = Optic("ITMY")
etmy = Optic("ETMY")
prm = Optic("PRM")
srm = Optic("SRM")
pr2 = Optic("PR2")

#-------------------------------------------------------------------------------
# Nodes to manage
imc_node = Node("IMC_LOCK")
xarm_node = Node("ALIGN_XARM")
yarm_node = Node("ALIGN_YARM")
irxarm_node = Node("ALIGN_IRXARM")
bs_node = Node("ALIGN_BS")
srx_node = Node("ALIGN_SRX")
prx_node = Node("ALIGN_PRX")

alsx_node = Node("ALS_XARM")
alsy_node = Node("ALS_YARM")

#nodes = NodeManager([
#    "IMC_LOCK",
#    "ALIGN_XARM",
#    "ALIGN_YARM",
#    "ALIGN_IRXARM",
#    "ALIGN_BS",
#    "ALIGN_SRX",
#    "ALIGN_PRX",
#])

#
#-------------------------------------------------------------------------------
#Functions:

def IMC_10W():
    return (imc_node.arrived and (ezca['IMC-IM4_TRANS_SUM_OUTPUT']>9) and (imc_node.state == 'LOCKED_10W'))


def realign_all_optics():
    for dof in ['P','Y']:
        itmx.align(dof, ezca)
        itmy.align(dof, ezca)
        etmx.align(dof, ezca)
        etmy.align(dof, ezca)
        prm.align(dof, ezca)
        srm.align(dof,ezca)
        pr2.align(dof,ezca)

def restore_corner():
    prm.restore(ezca)
    srm.restore(ezca)

def restore_arms():
    itmx.restore(ezca)
    itmy.restore(ezca)
    etmx.restore(ezca)
    etmy.restore(ezca)

#
#-------------------------------------------------------------------------------
#Classes:
#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#                                 Guardian States
#-------------------------------------------------------------------------------
#
class INIT(GuardState):
    request = False
    index = 0
    
    def main(self):
        #Does nothing.
        return True
    
#
#-------------------------------------------------------------------------------
'''
class INIT_ALIGN_ERROR(GuardState):
    #Indicates that there has been an error.
    
    goto = True #Can jump to the alignment error state.
    request = True #
    #index = ;
    
    def main(self):
        #
        pass;
    #
    
    def run(self):
        #
        #return("INIT_ALIGN_START");
        return True
'''
#
#-------------------------------------------------------------------------------
#
class INITIAL_ALIGN_RESET(GuardState):
    request = False
    goto = True
    index = 1
    
    def main(self):

        realign_all_optics()

        # Ensure all nodes are set back to idle
        xarm_node.set_request("ARM_ALIGN_IDLE")
        yarm_node.set_request("ARM_ALIGN_IDLE")
        irxarm_node.set_request("IR_ALIGN_IDLE")
        bs_node.set_request("BS_ALIGN_IDLE")
        srx_node.set_request("SRX_ALIGN_IDLE")
        prx_node.set_request("PRX_ALIGN_IDLE")

    def run(self):
        #
        return True


#-------------------------------------------------------------------------------
#
class INITIAL_ALIGN_IDLE(GuardState):
    request = True
    index = 2
    
    def run(self):
        
        return True

#-------------------------------------------------------------------------------
#
class RESTORE_OPTICS(GuardState):
    request = False
    index = 4

    #def main(self):

    #    restore_arms()
    #    restore_corner()

    def run(self):

        return True

# TODO: make this more of a check. Check that the current offset is close to the saved value.

#-------------------------------------------------------------------------------

class SET_POWER_TO_10W(GuardState):
    request = False #
    index = 5
    
    def main(self):
        
        # Steal control?
        imc_node.set_managed()
        # Request 10W (maybe better to use IFO POWER state?)
        imc_node.set_request("LOCKED_10W")
        # Release control, no need to micromanage
        imc_node.release()

        # No need to wait for this to arrive, the individual guardians will check
        return True
    
    def run(self):
        #
        return True

#-------------------------------------------------------------------------------
#
class ALIGN_GRN_ARMS(GuardState):    
    request = False
    index = 8
    
    def main(self):

        # Misalign PR2 so that IR doesn't mess up BPD signals
        pr2.misalign('P', -500, ezca)

        xarm_node.set_request("ALIGN_GRN_MANUAL")
        yarm_node.set_request("ALIGN_GRN_MANUAL")

    def run(self):

        if not (xarm_node.arrived and yarm_node.arrived):
            return

        return True
#
#-------------------------------------------------------------------------------
#
class ALIGN_GRN_MANUAL(GuardState):
    request = True
    index = 9

    def run(self):

        notify("Align ETMs and TMSs to optimize cavities and save.") 
        notify("Then request ARM_ALIGN_IDLE from ALIGN_XARM and ALIGN_YARM.")

        #FIXME: arrived is only true if the request came from the manager
        if not (xarm_node.state == "ARM_ALIGN_IDLE" and yarm_node.state == "ARM_ALIGN_IDLE"):
            notify("XARM and/or YARM guardian not in IDLE state")
            return

        #if not (yarm_node.state == "ARM_ALIGN_IDLE"): # and yarm_node.arrived):
        #    notify("YARM guardian not in IDLE state")
        #    return

        return True

#-------------------------------------------------------------------------------
#
class ALIGN_GRN_AUTO(GuardState):
    request = True
    index = 10

    def main(self):

        # Misalign PR2 so that IR doesn't mess up BPD signals
        pr2.misalign('P', -500, ezca)

        xarm_node.set_request("GRN_CAVITY_ALIGNED")
        yarm_node.set_request("GRN_CAVITY_ALIGNED")

    def run(self):

        if not (xarm_node.arrived and yarm_node.arrived):
            log("Waiting on arms to be aligned with green.")
            return

        return True


#-------------------------------------------------------------------------------
#
class ARMS_IDLE(GuardState):
    request = False #
    index = 11

    def main(self):
        self.idle = "ARM_ALIGN_IDLE"
        xarm_node.set_request(self.idle)
        yarm_node.set_request(self.idle)

    def run(self):

        notify("Returning ALIGN_XARM and ALIGN_YARM to IDLE state")

        if not (xarm_node.arrived and xarm_node.state == self.idle and yarm_node.arrived and yarm_node.state == self.idle):
            return

        return True


#-------------------------------------------------------------------------------
#
'''
class GRN_ARMS_ALIGNED(GuardState):
    request = True
    index = 10
    
    def run(self):
        
        return True
'''
#-------------------------------------------------------------------------------
#
class SKIP_ARMS(GuardState):
    index = 15


#-------------------------------------------------------------------------------
#
class RESTORE_RMS(GuardState):
    request = False
    index = 16

    def main(self):

        restore_corner()

    def run(self):

        return True

#-------------------------------------------------------------------------------

class ALIGN_IRX(GuardState):
    request = False
    index = 18
    
    def main(self):

        # Coarse realign PR2
        pr2.align('P', ezca)

        irxarm_node.set_request("IR_CAV_ALIGNED")
    
    def run(self):
        
        notify("Aligning IM4 and PR2 to XARM")

        if not (irxarm_node.arrived and irxarm_node.state == "IR_CAV_ALIGNED"):
            return

        return True
#
#-------------------------------------------------------------------------------
#
class IRX_ALIGNED(GuardState):
    request = True #
    index = 19
    
    def run(self):

        notify("IM4 and PR2 aligned")

        return True
#
#-------------------------------------------------------------------------------
#
class IRX_IDLE(GuardState):
    request = False #
    index = 20

    def main(self):
        self.idle = "IR_ALIGN_IDLE"
        irxarm_node.set_request(self.idle)

    def run(self):

        notify("Returning ALIGN_IRXARM to IDLE state")

        if not (irxarm_node.arrived and irxarm_node.state == self.idle):
            return

        return True
#
#-------------------------------------------------------------------------------
#
class ALIGN_BS(GuardState):
    request = False
    index = 28
    
    def main(self):

        self.align = "BS_ALIGNED"
        bs_node.set_request(self.align)
    
    def run(self):

        notify("Watch ALIGN_BS guardian")

        if not (bs_node.arrived and bs_node.state == self.align):
            return

        return True
#
#-------------------------------------------------------------------------------
#
class BS_ALIGNED(GuardState):
    request = True
    index = 29
    
    def run(self):

        notify("BS alignment complete")

        return True

#-------------------------------------------------------------------------------
#
class BS_IDLE(GuardState):
    request = False
    index = 30

    def main(self):
        self.idle = "BS_ALIGN_IDLE"
        bs_node.set_request(self.idle)

    def run(self):

        notify("Returning ALIGN_BS to IDLE state")

        if not (bs_node.arrived and bs_node.state == self.idle):
            return

        return True

#
#-------------------------------------------------------------------------------
#
class ALIGN_SRX(GuardState):
    request = False
    index = 38
    
    def main(self):

        self.align = "SRM_ALIGNED"
        srx_node.set_request(self.align)
    
    def run(self):

        notify("Aligning SR2 and SRM, watch ALIGN_SRX guardian")

        if not (srx_node.arrived and srx_node.state == self.align):
            return

        return True
#
#-------------------------------------------------------------------------------
#
class SRX_ALIGNED(GuardState):
    request = True
    index = 39
    
    def run(self):

        notify("SR2 and SRM alignment complete")

        return True

#-------------------------------------------------------------------------------
#
class SRX_IDLE(GuardState):
    request = False
    index = 40

    def main(self):
        self.idle = "SRX_ALIGN_IDLE"
        srx_node.set_request(self.idle)

    def run(self):

        notify("Returning ALIGN_SRX to IDLE state")

        if not (srx_node.arrived and srx_node.state == self.idle):
            return

        return True

#-------------------------------------------------------------------------------
#
class ALIGN_PRX(GuardState):
    request = False
    index = 48
    
    def main(self):

        self.align = "PRM_ALIGNED"
        prx_node.set_request(self.align)
    
    def run(self):

        notify("Aligning PRM, watch ALIGN_PRX guardian")

        if not (prx_node.arrived and prx_node.state == self.align):
            return

        return True
#
#-------------------------------------------------------------------------------
#
class PRX_ALIGNED(GuardState):
    request = True
    index = 49
    
    def run(self):

        notify("PRM alignment complete")

        return True

#-------------------------------------------------------------------------------
#
class PRX_IDLE(GuardState):
    request = False
    index = 50

    def main(self):
        self.idle = "PRX_ALIGN_IDLE"
        prx_node.set_request(self.idle)

    def run(self):

        notify("Returning ALIGN_PRX to IDLE state")

        if not (prx_node.arrived and prx_node.state == self.idle):
            return

        return True
#
#-------------------------------------------------------------------------------
#
class SET_POWER_TO_1W(GuardState):
    request = False
    index = 55

    def main(self):
        
        # Request 1W 
        imc_node.set_request("LOCKED_1W")
        # Release control, no need to micromanage
        imc_node.release()

        return True
    
    def run(self):
        #
        return True
#
#-------------------------------------------------------------------------------
#
class REALIGN_OPTICS(GuardState):
    request = False
    index = 57

    def main(self):

        ezca['SYS-MOTION_C_FASTSHUTTER_A_BLOCK'] = 1

        realign_all_optics()
        
    def run(self):
        return True

#-------------------------------------------------------------------------------
#
class ALIGNMENT_COMPLETE(GuardState):
    request = True
    index = 60
    
    def run(self):
        
        notify("The IFO has been aligned")
        return True

#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#                                    Edges
#-------------------------------------------------------------------------------
edges = [
         ## Recovery
         ("INIT", "INITIAL_ALIGN_IDLE"),
         ("INITIAL_ALIGN_RESET", "INITIAL_ALIGN_IDLE"),
         ## Branches
         ("INITIAL_ALIGN_IDLE", "RESTORE_OPTICS"),
         ("INITIAL_ALIGN_IDLE", "SKIP_ARMS", 4),
         ("SKIP_ARMS", "RESTORE_RMS"),
         ("RESTORE_RMS","ALIGN_IRX"),
         ("RESTORE_OPTICS", "SET_POWER_TO_10W"),
         ("SET_POWER_TO_10W", "ALIGN_GRN_ARMS", 3),
         ("SET_POWER_TO_10W", "ALIGN_GRN_AUTO"),
         ("ALIGN_GRN_AUTO", "ARMS_IDLE"),
         ("ARMS_IDLE", "ALIGN_IRX"),
         ("ALIGN_GRN_ARMS", "ALIGN_GRN_MANUAL"),
         ("ALIGN_GRN_MANUAL", "ALIGN_IRX"),
         ("ALIGN_IRX", "IRX_ALIGNED"),
         ("IRX_ALIGNED", "IRX_IDLE"),
         ("IRX_IDLE", "ALIGN_BS"),
         ("ALIGN_BS", "BS_ALIGNED"),
         ("BS_ALIGNED", "BS_IDLE"),
         ("BS_IDLE", "ALIGN_SRX"),
         ("ALIGN_SRX", "SRX_ALIGNED"),
         ("SRX_ALIGNED", "SRX_IDLE"),
         ("SRX_IDLE", "ALIGN_PRX"),
         ("ALIGN_PRX", "PRX_ALIGNED"),
         ("PRX_ALIGNED", "PRX_IDLE"),
         ("PRX_IDLE", "SET_POWER_TO_1W"),
         ("SET_POWER_TO_1W", "REALIGN_OPTICS"),
         ("REALIGN_OPTICS", "ALIGNMENT_COMPLETE"),
        ];
    #
#
#-------------------------------------------------------------------------------
#END.
